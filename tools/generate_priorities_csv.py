
from operator import itemgetter, attrgetter, methodcaller
import pickle
import gitlab
import re
import csv
import os
from datetime import date
import datetime
import json
import requests
from pprint import pprint
import csv, io


if "GITLAB_TOKEN" not in os.environ or "ZOHO_TOKEN" not in os.environ:
    print("""usage: GITLAB_TOKEN="yourtoken" ZOHO_TOKEN="yourtoken" python test.py""")
    print("")
    print("Generate a gitlab token from the parameters of you Gitlab accounts.")
    print("Generate a zoho doc token https://sheet.zoho.com/help/api/v2/#UPDATE-Set-content-to-a-range. It is only temporary.")
    print("")
    print("Write everything to the following doc: https://docs.zoho.com/sheet/open/5q1eqc1f905b1479449e48c36541dfda1b805")
    exit()
GITLAB_TOKEN = os.environ["GITLAB_TOKEN"]
ZOHO_TOKEN = os.environ["ZOHO_TOKEN"]


#---------- some zoho functions

def set_cell_content(sheet_id, worksheet_name, col, row, content):
    req = requests.post(f"https://sheet.zoho.com/api/v2/{sheet_id}",
                    data=
                        {
                            "method":"cell.content.set",
                            "worksheet_name":"",
                            "worksheet_name":worksheet_name,
                            "row":row,"column":col,
                            "content":content},
                    headers={"Authorization":f"Zoho-oauthtoken {ZOHO_TOKEN}",
                            "content-type": "application/x-www-form-urlencoded;charset=UTF-8"})
    #pprint(req.content)
    try:
        r = req.json()
        if r["status"] != "success":
            raise Exception("request error")
    except:
        pprint(req.content)
        exit()
    return True


def xy2range(col, row):
    alphabet = "?ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return f"{alphabet[col]}{row}:{alphabet[col]}{row}"


def set_sell_color(sheet_id, worksheet_name, col, row, style, color):
    cell_range = xy2range(col, row)
    if style not in ["font_color", "fill_color"]:
        raise Exception("arg")
    req = requests.post(f"https://sheet.zoho.com/api/v2/{sheet_id}",
                    data=
                        {
                            "method":"ranges.format.set",
                            "format_json": '[{"worksheet_name":"'+worksheet_name+'", "range":"'+cell_range+'", "'+style+'":"'+color+'"}]'},
                    headers={"Authorization":f"Zoho-oauthtoken {ZOHO_TOKEN}",
                            "content-type": "application/x-www-form-urlencoded;charset=UTF-8"})
    try:
        r = req.json()
        if r["status"] != "success":
            raise Exception("request error")
    except:
        pprint(req.content)
        exit()
    return True


def create_worksheet(sheet_id, worksheet_name):
    req = requests.post(f"https://sheet.zoho.com/api/v2/{sheet_id}",
                    data=
                        {
                            "method":"worksheet.insert",
                            "worksheet_name": worksheet_name
                        },
                    headers={"Authorization":f"Zoho-oauthtoken {ZOHO_TOKEN}",
                            "content-type": "application/x-www-form-urlencoded;charset=UTF-8"})
    try:
        r = req.json()
        if r["status"] != "success":
            raise Exception("request error")
    except:
        pprint(req.content)
        exit()
    return r["new_worksheet_name"]


def set_range_content(sheet_id, worksheet_name, col, row, csv_content):
    req = requests.post(f"https://sheet.zoho.com/api/v2/{sheet_id}",
                    data=
                        {
                            "method":"worksheet.csvdata.set",
                            "worksheet_name":worksheet_name,
                            "row":row,"column":col,
                            "ignore_empty": False,
                            "data": csv_content
                        },
                    headers={"Authorization":f"Zoho-oauthtoken {ZOHO_TOKEN}",
                            "content-type": "application/x-www-form-urlencoded;charset=UTF-8"})
    try:
        r = req.json()
        if r["status"] != "success":
            raise Exception("request error")
    except:
        pprint(req.content)
        exit()
    return True


#----------



try:
    print("try loading pickle")
    issues_from_gitlab = pickle.load(open("issues.pkl", "rb"))
except FileNotFoundError:
    print("nop, loading from gitlab")
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=GITLAB_TOKEN)
    gl.auth()

    p = gl.projects.get("ryax-tech/dev/roadmap")
    issues_from_gitlab = p.issues.list(all=True, state='opened')

    #print("dumping")
    #pickle.dump(issues_from_gitlab, open("issues.pkl", "wb"))
print("init done")

#i = p.issues.list()
#d = i[5].discussions.list()[1]

issues = []

priorities = {
    "CRITICAL": 100,
    "High": 20,
    "Medium": 5,
    "No priority": 1,
    "Very Low": 0
}
priorities_color = {
    "CRITICAL": "#c83771",
    "High": "#d35f8d",
    "Medium": "#de87aa",
    "No priority": "#ffffff",
    "Very Low": "#f4d7e3"
}
states = {
    "Thinking": 5,
    "Defined": 4,
    "PoCed": 3,
    "Beta": 2,
    "Released": 1,
    "Dead": 0
}
states_color = {
    "Thinking": "#e5e6e4",
    "Defined": "#eef4d7",
    "PoCed": "#dde9af",
    "Beta": "#cdde87",
    "Released": "#bcd35f",
    "Dead": "#cccccc"
}


def find_fmn(t):
    return re.findall(r"Feature Marketing Name: ([^/]+)/([^\n]+)", t)

def find_characteristic(txt, characteristic):
    r = re.findall(re.escape(characteristic)+r"([^\n]+)", txt)
    if len(r) == 0:
        return ""
    else:
        return [x.strip() for x in r][-1]

for issue in issues_from_gitlab:
    ret = {}
    ret["title"] = issue.title
    ret["id"] = issue.iid
    ret["labels"] = issue.labels
    ret["web_url"] = issue.web_url
    ret["linked_issues"] = []
    ret["fmns"] = find_fmn(issue.description)

    ret["deadline_defined"] = find_characteristic(issue.description, "Deadline for ~Defined:")
    ret["deadline_mocked"] = find_characteristic(issue.description, "Deadline for ~Mocked:")
    ret["deadline_poced"] = find_characteristic(issue.description, "Deadline for ~PoCed:")
    ret["deadline_released"] = find_characteristic(issue.description, "Deadline for ~Released:")
    ret["difficulty"] = find_characteristic(issue.description, "Difficulty of releasing this:")
    ret["good_for"] = find_characteristic(issue.description, "What is it good for?")
    ret["helps"] = find_characteristic(issue.description, "This feature helps:")
    ret["tech_priority"] = find_characteristic(issue.description, "Technical priority:")
    ret["biz_priority"] = find_characteristic(issue.description, "Business priority:")
    ret["biz_metric"] = find_characteristic(issue.description, "Business metric:")
    ret["biz_commitment"] = find_characteristic(issue.description, "Business metric commitment:")

    #sorting using importance score
    ret["importance"] = 0
    if ret["tech_priority"].isnumeric():
        ret["importance"] += int(ret["tech_priority"])*10
    if ret["biz_priority"].isnumeric():
        ret["importance"] += int(ret["biz_priority"])*10
    for p, s in priorities.items():
        if p in ret["labels"]:
            ret["priority"] = p
            ret["importance"] += s*100
    for p, s in states.items():
        if p in ret["labels"]:
            ret["state"] = p
            ret["importance"] += s

    for note in issue.notes.list(all=True):
        if note.body.startswith("mentioned in issue "):
            linked_issue = note.body[len("mentioned in issue "):]
            ret["linked_issues"].append(linked_issue)
        else:
            ret["fmns"] += find_fmn(note.body)

    if len(ret["fmns"]) == 0:
        ret["fmns"] = [("general", ret["title"])]
    
    issues.append(ret)


issues = sorted(issues, key=itemgetter("importance"), reverse=True)

print(issues)
for issue in issues:
    print("-----")
    print("#", issue["title"])
    print("`"+"`, `".join(issue["labels"])+"`")
    if "priority" not in issue:
        print("WARNING: no priority defined!")
    if "state" not in issue:
        print("WARNING: no state defined!")
    print(issue["linked_issues"])


t = date.today()
with open(f"features_priorities_{t.year}-{t.month}-{t.day}.csv", 'w', newline='') as csvfile:
    f = csv.writer(csvfile)
    f.writerow(["Importance", "Priority", "Issue", "Title", "State",
                "Deadline for ~Defined:",
                "Deadline for ~Mocked:",
                "Deadline for ~PoCed:",
                "Deadline for ~Released:",
                "Difficulty of releasing this:",
                "What is it good for?",
                "This feature helps:",
                "Technical priority:",
                "Business priority:",
                "Business metric:",
                "Business metric commitment:",
                ])
    for issue in issues:
        labels = issue['labels']
        for l in states.keys():
            try:
                labels.remove(l)
            except ValueError:
                pass
        for l in priorities.keys():
            try:
                labels.remove(l)
            except ValueError:
                pass
        if "priority" not in issue:
            issue['priority'] = "UNKNWON"
        if "state" not in issue:
            issue['state'] = "UNKNWON"

        f.writerow([
            issue['importance'],
            issue['priority'],
            issue['id'],
            issue["title"],
            issue['state'],
            issue["deadline_defined"],
            issue["deadline_mocked"],
            issue["deadline_poced"],
            issue["deadline_released"],
            issue["difficulty"],
            issue["good_for"],
            issue["helps"],
            issue["tech_priority"],
            issue["biz_priority"],
            issue["biz_metric"],
            issue["biz_commitment"],
            ])



sheet_id = "5q1eqc1f905b1479449e48c36541dfda1b805"
t = datetime.datetime.today()
worksheet_name = f"{t.year}-{t.month}-{t.day}_{t.hour}-{t.minute}--{t.second}"
create_worksheet(sheet_id, worksheet_name)

ROW = 1

csv_content = io.StringIO()
csv_writer = csv.writer(csv_content)

def writerow(l):
    global ROW
    i = 1
    to_write = []
    for e in l:
        if isinstance(e, tuple):
            set_sell_color(sheet_id, worksheet_name, i, ROW, "fill_color", e[1])
            content = e[0]
        else:
            content = str(e)
        to_write.append(content)
        i += 1
    csv_writer.writerow(to_write)
    ROW += 1


labels_color = {
    "Edge": "#5f8dd3",
    "BUG": "#ff0000",
    "Industry ready": "#aa87de",
    }

c = "#BFBFBF"
writerow([
    ("Importance", c),
    ("Priority", c),
    ("Issue", c),
    ("Title", c),
    ("State", c),
    ("Technical priority:", c),
    ("Business priority:", c),
    ("Difficulty of releasing this:", c),
    ("Deadline for ~Defined:", c),
    ("Deadline for ~Mocked:", c),
    ("Deadline for ~PoCed:", c),
    ("Deadline for ~Released:", c),
    ("What is it good for?", c),
    ("This feature helps:", c),
    ("Business metric:", c),
    ("Business metric commitment:", c),
    ])
for issue in issues:
    labels = []
    for l in issue['labels']:
        if l in states.keys():
            continue
        if l in priorities.keys():
            continue
        if l in labels_color.keys():
            labels.append( (l, labels_color[l]) )
        else:
            labels.append( l )
    
    if "priority" not in issue or issue['priority'] == "UNKNWON":
        issue['priority'] = ("UNKNWON", "#ff0000")
    else:
        issue['priority'] = (issue['priority'], priorities_color[issue['priority']])

    if "state" not in issue or issue['state'] == "UNKNWON":
        issue['state'] = ("UNKNWON", "#ff0000")
    else:
        issue['state'] = (issue['state'], states_color[issue['state']])

    writerow([
        issue['importance'],
        issue['priority'],
        issue['id'],
        issue['title'],
        issue['state'],
        issue["tech_priority"],
        issue["biz_priority"],
        issue["difficulty"],
        issue["deadline_defined"],
        issue["deadline_mocked"],
        issue["deadline_poced"],
        issue["deadline_released"],
        issue["good_for"],
        issue["helps"],
        issue["biz_metric"],
        issue["biz_commitment"],
        ]
    )

set_range_content(sheet_id, worksheet_name, 1, 1, csv_content.getvalue().strip('\r\n'))


