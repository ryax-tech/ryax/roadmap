# Action

As a {persona}, I want to ... , so that ... .

<!-- {persona} is one of: Backend Developer, Data Engineer, Data Scientist, Ryax team -->

Is this a default action? {**yes**, **no**}

Kind: {source, processor, pubisher}

Inputs:
- Displayed name (type): description
- ...
Outputs:
-  Displayed name (type): description
- ...

<!--
Write down what the user of this action will do with this action
- what he can do with it?
- what are the limits?
- what are the steps to use this feature?
- how this User Story can be demonstrated?
It should be as close as possible to a recipe

If there is some user interactions, describes them, don't forget the error cases.
-->

## Recipes and tests

<!--
Describes steps to use and test this action
Describes steps to test the action
-->

/label ~"IDEA"
/label ~"Action"
/label ~"Triage"
/assign @mercierm
