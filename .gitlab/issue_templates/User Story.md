# User Story

**As a {persona}, I want to ... , so that ... .**

<!-- {persona} is one of: Business user, Creator, Admin, Us (only one, the major one) -->

<!-- 
Write down what the user of this feature will do to use this feature:
- what he can do with it?
- what are the limits?
- what are the steps to use this feature?
- how this User Story can be demonstrated?
It should be the closer possible to a recipe
If there is some user interactions, describes them, don't forget the error cases.
-->

## Recipes and tests

<!-- 
Describes steps to perform the US
- On screen ... , when I click on ..., ... happen and ... is displayed
- if the condition ... happen, then ... 
- If the input ... is wrong, then ...

Describes steps to test the feature
- If I click twice on ..., then ...
-->


## Is it a breaking change? Why?

<!-- 
Choose any of the following:
This is not a breaking change and the release note is enough
This is a breaking change because it breaks:
- the API
- the internal messages
- the Database schema (no migration available)
-->


## What is it good for?

<!-- 
Choose one of the following:
On-boarding: it will help people enter into the Ryax world
Delight: users and developers will love this feature
Security: improve Ryax security
Fun: we will like doing this
-->


/label ~"IDEA"
/label ~"UserStory"
/label ~"Triage"
/assign @mercierm
