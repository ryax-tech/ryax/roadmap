
<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!-- How one can reproduce the issue - this is very important  -->

### What is the current *bug* behavior and the expected *correct* behavior?

<!-- What actually happens -->

### Relevant logs and/or screenshots

<!--
Paste any relevant screenshots and logs here with some context.
Please use `code blocks` to format console output, logs, and code.
-->

```
<this is a code block example>
```

### Link to the Ryax instance

<!--  Paste the link of the Ryax instance and the relevant pages if any -->

### Possible fixes

<!--
Suggestions for fixes if you have one
Also add a link to the line of codes that might be responsible for the problem if you have it.
 -->


/label ~BUG
/label ~Triage
/assign @mercierm
